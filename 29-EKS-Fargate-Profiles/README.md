---
title: AWS EKS Fargate Profile with Terraform
description: Create AWS EKS Kubernetes Fargate Profiles with Terraform
---
## Step-01: Introduction
- Create AWS EKS Fargate Profile

## Step-02: Review Terraform manifests
- Create DynamoDB Table `dev-eks-fargate-profile`

## Step-03: c4-02-kubernetes-namespace.tf
```t
# Resource: Kubernetes Namespace fp-ns-app1
resource "kubernetes_namespace_v1" "fp_ns_app1" {
  metadata {
    name = "fp-ns-app1"
  }
}
```

## Step-04: c5-01-fargate-profile-iam-role-and-policy.tf
```t
# Resource: IAM Role for EKS Fargate Profile
resource "aws_iam_role" "fargate_profile_role" {
  name = "${local.name}-eks-fargate-profile-role-apps"

  assume_role_policy = jsonencode({
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Principal = {
        Service = "eks-fargate-pods.amazonaws.com"
      }
    }]
    Version = "2012-10-17"
  })
}

# Resource: IAM Policy Attachment to IAM Role
resource "aws_iam_role_policy_attachment" "eks_fargate_pod_execution_role_policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSFargatePodExecutionRolePolicy"
  role       = aws_iam_role.fargate_profile_role.name
}
```

## Step-05: c5-02-fargate-profile.tf
```t
# Resource: EKS Fargate Profile
resource "aws_eks_fargate_profile" "fargate_profile" {
  cluster_name           = data.terraform_remote_state.eks.outputs.cluster_id
  fargate_profile_name   = "${local.name}-fp-app1"
  pod_execution_role_arn = aws_iam_role.fargate_profile_role.arn
  subnet_ids = data.terraform_remote_state.eks.outputs.private_subnets
  selector {
    namespace = "fp-ns-app1"
  }
}
```


## Step-06: c5-03-fargate-profile-outputs.tf
```t
# Fargate Profile Outputs
output "fargate_profile_arn" {
  description = "Fargate Profile ARN"
  value = aws_eks_fargate_profile.fargate_profile.arn 
}

output "fargate_profile_id" {
  description = "Fargate Profile ID"
  value = aws_eks_fargate_profile.fargate_profile.id 
}

output "fargate_profile_status" {
  description = "Fargate Profile Status"
  value = aws_eks_fargate_profile.fargate_profile.status
}
```

## Step-07: Execute Terraform Commands
```t

# Terraform Initialize
terraform init

# Terraform Validate
terraform validate

# Terraform Plan
terraform plan

# Terraform Apply
terraform apply -auto-approve

# Configure kubeconfig for kubectl
aws eks --region <region-code> update-kubeconfig --name <cluster_name>
aws eks --region us-east-1 update-kubeconfig --name hr-dev-eksdemo1

# Verify Kubernetes Worker Nodes using kubectl
kubectl get nodes
kubectl get nodes -o wide
```

## Step-08: Verify Fargate Profile using AWS CLI
- [AWS EKS CLI](https://awscli.amazonaws.com/v2/documentation/api/2.1.29/reference/eks/index.html)
```t
# List Fargate Profiles
aws eks list-fargate-profiles --cluster <CLUSTER_NAME>
aws eks list-fargate-profiles --cluster hr-dev-eksdemo1
```
## Step-09: Review aws-auth ConfigMap for Fargate Profiles related Entry
- When AWS Fargate Profile is created on EKS Cluster, `aws-auth` configmap is updated in EKS Cluster with the IAM Role we are using for Fargate Profiles. 
- For additional reference, review file: `sample-aws-auth-configmap.yaml`
```t
# Review the aws-auth ConfigMap
kubectl -n kube-system get configmap aws-auth -o yaml

## Sample from aws-auth ConfigMap related to Fargate Profile IAM Role
    - groups:
      - system:bootstrappers
      - system:nodes
      - system:node-proxier
      rolearn: arn:aws:iam::180789647333:role/hr-dev-eks-fargate-profile-role-apps
      username: system:node:{{SessionName}}
```

## Step-10: Verify Fargate Profile using AWS Mgmt Console
```t
# Get the current user configured in AWS CLI (EKS Cluster Creator user)
aws sts get-caller-identity

## Sample Output
aws sts get-caller-identity
{
    "UserId": "AIDASUF7HC7SSJRDGMFBM",
    "Account": "180789647333",
    "Arn": "arn:aws:iam::180789647333:user/prashant"
}

# Verify Fargate Profiles via AWS Mgmt Console
1. Login to AWS Mgmt console with same user with which we are created the EKS Cluster.
2. Go to Services -> Elastic Kubernetes Services -> Clusters -> hr-dev-eksdemo1
3. Go to "Configuration" Tab -> "Compute Tab"   
4. Review the Fargate profile in "Fargate profiles" section
```







