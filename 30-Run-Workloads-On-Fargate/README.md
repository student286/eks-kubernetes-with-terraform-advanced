---
title: AWS EKS Fargate Profile with Terraform
description: Create AWS EKS Kubernetes Fargate Profiles with Terraform
---
## Step-01: Introduction
- Run EKS Workloads on AWS Fargate

## Step-02: Review Terraform Manifests
- Update all the Kubernetes Resources (Deployments, Node Port Services, Ingress Service) with ` namespace = "fp-ns-app1"`.
- All the Kubernetes Workloads (App1, App2 and App3) pods will be scheduled on Fargate Nodes

```t
# Custom Requests for Fargate Pods in a Kubernetes Deployment 
          resources {
            requests = {
              "cpu" = "1000m"
              "memory" = "2048Mi" 
            }
            limits = {
              "cpu" = "2000m"
              "memory" = "4096Mi"
            }
          }
```

## Step-03: Execute Terraform Commands
```t

# Terraform Initialize
terraform init

# Terraform Validate
terraform validate

# Terraform Plan
terraform plan

# Terraform Apply
terraform apply -auto-approve
```

## Step-04: Verify Kubernetes Resources
```t
# Verify Nodes
kubectl get nodes
Observation:
1. New Fargate nodes will be created

# Verify Deployments
kubectl -n fp-ns-app1 get deploy

# Verify Pods
kubectl -n fp-ns-app1 get pods

# Verify Services
kubectl -n fp-ns-app1 get svc

# Verify Ingress Service
kubectl -n fp-ns-app1 get ingress

# Access Application
http://fargate.architectprashant.com
http://fargate.architectprashant.com/app1/index.html
http://fargate.architectprashant.com/app2/index.html
```

## Step-05: Review Pod Memory and CPU - Default Allocated
- [Fargate Pod vCPU Value vs Memory Value](https://docs.aws.amazon.com/eks/latest/userguide/fargate-pod-configuration.html)
```t
# List Pods
kubectl -n fp-ns-app1 get pods

# Describe Pod
kubectl -n fp-ns-app1 describe pod <APP1-POD-NAME>
kubectl -n fp-ns-app1 describe pod app1-nginx-deployment-777cddb9b4-rhrpq
Observation:
1. Review Annotations section

# Sample: Default Capacity Allocated
Annotations:          CapacityProvisioned: 0.25vCPU 0.5GB
```
## Step-06: Review Pod Memory and CPU - Custom Kubernetes Requests and Limits
```t
# List Pods
kubectl -n fp-ns-app1 get pods

# Describe Pod
kubectl -n fp-ns-app1 describe pod <APP3-POD-NAME>
kubectl -n fp-ns-app1 describe pod app3-nginx-deployment-b54c5b6bd-m6hh9
Observation:
1. Review Annotations section

# Sample: Customized Capacity Allocated for Fargate Pod
CapacityProvisioned: 1vCPU 3GB
```

## Step-07: Clean-Up
```t

# Destroy Resources
terraform apply -destroy -auto-approve

```

## Step-08: Clean-Up EKS Cluster, LBC Controller, ExternalDNS and Fargate Profile
- Destroy Resorces Order
  - 04-fargate-profiles-terraform-manifests
  - 03-externaldns-install-terraform-manifests
  - 02-lbc-install-terraform-manifests
  - 01-ekscluster-terraform-manifests
```t
##############################################################
## Delete Fargate Profile
# Change Directory
cd 04-fargate-profiles-terraform-manifests

# Terraform Destroy
terraform init
terraform apply -destroy -auto-approve
##############################################################
## Destroy External DNS
# Change Directroy
cd 03-externaldns-install-terraform-manifests

# Terraform Destroy
terraform init
terraform apply -destroy -auto-approve
##############################################################
## Destroy  LBC
# Change Directroy
cd 02-lbc-install-terraform-manifests

# Terraform Destroy
terraform init
terraform apply -destroy -auto-approve
##############################################################
## Destroy EKS Cluster
# Change Directroy
cd 01-ekscluster-terraform-manifests

# Terraform Destroy
terraform init
terraform apply -destroy -auto-approve
##############################################################
```



