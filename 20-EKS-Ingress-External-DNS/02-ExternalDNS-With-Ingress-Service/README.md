---
title: AWS EKS Ingress and ExternalDNS with Terraform
description: Update AWS Route53 records using ExternalDNS in Ingress Service on AWS EKS Cluster
---


## Step-01: Update Ingress manifest by adding External DNS Annotation
- Added annotation with two DNS Names
  - dnstest101.architectprashant.com
  - dnstest102.architectprashant.com
- Once we deploy the application, we should be able to access our Applications with both DNS Names.   
```yaml
    # External DNS - For creating a Record Set in Route53
    external-dns.alpha.kubernetes.io/hostname: dnstest101.architectprashant.com, dnstest102.architectprashant.com
```

## Step-02: Deploy all Application Kubernetes Manifests
### Deploy
```t
# Deploy kube-manifests
kubectl apply -f 04-kube-manifests-ingress-externaldns/

# Verify Ingress Resource
kubectl get ingress

# Verify Apps
kubectl get deploy
kubectl get pods

# Verify NodePort Services
kubectl get svc
```
### Verify Load Balancer & Target Groups
- Load Balancer -  Listeneres (Verify both 80 & 443) 
- Load Balancer - Rules (Verify both 80 & 443 listeners) 
- Target Groups - Group Details (Verify Health check path)
- Target Groups - Targets (Verify all 3 targets are healthy)

### Verify External DNS Log
```t
# Verify External DNS logs
kubectl logs -f $(kubectl get po | egrep -o 'external-dns[A-Za-z0-9-]+')
```
### Verify Route53
- Go to Services -> Route53
- You should see **Record Sets** added for `dnstest101.architectprashant.com`, `dnstest102.architectprashant.com`

## Step-04: Access Application using newly registered DNS Name
### Perform nslookup tests before accessing Application
- Test if our new DNS entries registered and resolving to an IP Address
```t
# nslookup commands
nslookup dnstest101.architectprashant.com
nslookup dnstest102.architectprashant.com
```
### Access Application using dnstest1 domain
```t
# HTTP URLs (Should Redirect to HTTPS)
http://dnstest101.architectprashant.com/app1/index.html
http://dnstest101.architectprashant.com/app2/index.html
http://dnstest101.architectprashant.com/
```

### Access Application using dnstest2 domain
```t
# HTTP URLs (Should Redirect to HTTPS)
http://dnstest102.architectprashant.com/app1/index.html
http://dnstest102.architectprashant.com/app2/index.html
http://dnstest102.architectprashant.com/
```


## Step-05: Clean Up
```t
# Delete Manifests
kubectl delete -f 04-kube-manifests-ingress-externaldns/

```

## Step-06: Review Terraform Manifests 


## Step-07: c10-kubernetes-ingress-service.tf
- One Annotation Addition in Ingress Service
```t
    # External DNS - For creating a Record Set in Route53
      "external-dns.alpha.kubernetes.io/hostname" = "dnstest101.architectprashant.com, dnstest102.architectprashant.com"
```

## Step-08: Execute Terraform Commands
```t

# Terraform Initialize
terraform init

# Terraform Validate
terraform validate

# Terraform Plan
terraform plan

# Terraform Apply
terraform apply -auto-approve

# Terraform Apply Refresh-Only
terraform apply -refresh-only -auto-approve
Observation: 

### SAMPLE OUTPUT ###
Changes to Outputs:
  ~ acm_certificate_status = "PENDING_VALIDATION" -> "ISSUED"
Outputs:
acm_certificate_arn = "arn:aws:acm:us-east-1:180789647333:certificate/06033cd0-3ecb-4069-8679-b54ea6678f5a"
acm_certificate_id = "arn:aws:acm:us-east-1:180789647333:certificate/06033cd0-3ecb-4069-8679-b54ea6678f5a"
acm_certificate_status = "ISSUED"
```

## Step-09: Verify Ingress Service
```t
# Verify Ingress Resource
kubectl get ingress

# Verify Apps
kubectl get deploy
kubectl get pods

# Verify NodePort Services
kubectl get svc
```

## Step-10: Verify External DNS Log
```t
# Verify External DNS logs
kubectl logs -f $(kubectl get po | egrep -o 'external-dns[A-Za-z0-9-]+')
```

## Step-11: Verify Route53
- Go to Services -> Route53
- You should see **Record Sets** added for `dnstest101.architectprashant.com`, `dnstest102.architectprashant.com`

## Step-12: Access Application using newly registered DNS Name
- Perform nslookup tests before accessing Application
- Test if our new DNS entries registered and resolving to an IP Address
```t
# nslookup commands
nslookup dnstest101.architectprashant.com
nslookup dnstest102.architectprashant.com
```
## Step-13: Access Application using tfdnstest1 and tfdnstest2 domains
```t
## Access Application using dnstest1 domain
# HTTP URLs (Should Redirect to HTTPS)
http://dnstest101.architectprashant.com/app1/index.html
http://dnstest101.architectprashant.com/app2/index.html
http://dnstest101.architectprashant.com/

## Access Application using dnstest2 domain
# HTTP URLs (Should Redirect to HTTPS)
http://dnstest102.architectprashant.com/app1/index.html
http://dnstest102.architectprashant.com/app2/index.html
http://dnstest102.architectprashant.com/
```


## Step-14: Clean-Up Ingress
```t

# Terraform Destroy
terraform apply -destroy -auto-approve

```

## References
- https://github.com/kubernetes-sigs/external-dns/blob/master/docs/tutorials/alb-ingress.md
- https://github.com/kubernetes-sigs/external-dns/blob/master/docs/tutorials/aws.md


