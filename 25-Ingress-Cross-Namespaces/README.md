---
title: AWS EKS Ingress Cross Namespaces with Terraform
description: AWS EKS Ingress Cross Namespaces with Terraform
---

## Step-01: Introduction
- Create Ingress Services in Multiple Namespaces and merged to create to a Single Application Load Balancer with Ingress Groups concept. 

## Step-02: Review App1 Ingress Manifest - Key Lines
```yaml
    # Ingress Groups
    alb.ingress.kubernetes.io/group.name: myapps.web
    alb.ingress.kubernetes.io/group.order: '10'
```

## Step-03: Review App2 Ingress Manifest - Key Lines
```yaml
    # Ingress Groups
    alb.ingress.kubernetes.io/group.name: myapps.web
    alb.ingress.kubernetes.io/group.order: '20'
```

## Step-04: Review App3 Ingress Manifest - Key Lines
```yaml
    # Ingress Groups
    alb.ingress.kubernetes.io/group.name: myapps.web
    alb.ingress.kubernetes.io/group.order: '30'
```

## Step-05: Create Namespaces
```yaml
# Namespace: ns-app1
apiVersion: v1
kind: Namespace
metadata: 
  name: ns-app1

# Namespace: ns-app2
apiVersion: v1
kind: Namespace
metadata: 
  name: ns-app2

# Namespace: ns-app3
apiVersion: v1
kind: Namespace
metadata: 
  name: ns-app3
```

## Step-06: Update Deployment, NodePort Service and Ingress Service as namespaced resources
- **Deployment and NodePort Service:** Update Namespace in resource metadata

- **Ingress Service:** Update Namespace in resource metadata


## Step-07: Deploy Apps with two Ingress Resources
```t
# Deploy both Apps
kubectl apply -R -f 04-kube-manifests-ingress-cross-ns/

# Verify Pods
kubectl get pods -n ns-app1
kubectl get pods -n ns-app2
kubectl get pods -n ns-app3

# Verify Ingress
kubectl  get ingress -n ns-app1
kubectl  get ingress -n ns-app2
kubectl  get ingress -n ns-app3
Observation:
1. Three Ingress resources will be created with same ADDRESS value
2. Three Ingress Resources are merged to a single Application Load Balancer as those belong to same Ingress group "myapps.web"
```

## Step-08: Verify on AWS Mgmt Console
- Go to Services -> EC2 -> Load Balancers 
- Verify Routing Rules for `/app1` and `/app2` and `default backend`

## Step-09: Verify by accessing in browser
```t
# Web URLs
http://crossns.architectprashant.com/app1/index.html
http://crossns.architectprashant.com/app2/index.html
http://crossns.architectprashant.com
```

## Step-10: Clean-Up
```t
# Delete Apps from k8s cluster
kubectl delete -R -f 04-kube-manifests-ingress-cross-ns/

## Verify Route53 Record Set to ensure our DNS records got deleted
- Go to Route53 -> Hosted Zones -> Records 
- The below records should be deleted automatically
  - crossns.architectprashant.com
```

## Step-11: Review Terraform Manifests 


## Step-12: c13-kubernetes-namespaces.tf
```t
# Resource: Kubernetes Namespace ns-app1
resource "kubernetes_namespace_v1" "ns_app1" {
  metadata {
    name = "ns-app1"
  }
}

# Resource: Kubernetes Namespace ns-app2
resource "kubernetes_namespace_v1" "ns_app2" {
  metadata {
    name = "ns-app2"
  }
}

# Resource: Kubernetes Namespace ns-app3
resource "kubernetes_namespace_v1" "ns_app3" {
  metadata {
    name = "ns-app3"
  }
}
```


## Step-13: Add Namespace for Deployments, NodePort and Ingress Service
```t
# Add it in every k8s resource Metadata section
# Sample
    namespace = kubernetes_namespace_v1.ns_app1.metadata[0].name    
```


## Step-14: Execute Terraform Commands
```t

# Terraform Initialize
terraform init

# Terraform Validate
terraform validate

# Terraform Plan
terraform plan

# Terraform Apply
terraform apply -auto-approve
```

## Step-15: Verify Ingress Service
```t
# Verify Ingress Resource
kubectl get ingress -n ns-app1
kubectl get ingress -n ns-app2
kubectl get ingress -n ns-app3

# Verify Apps
kubectl get deploy -n ns-app1
kubectl get deploy -n ns-app2
kubectl get deploy -n ns-app3
kubectl get pods -n ns-app1
kubectl get pods -n ns-app2
kubectl get pods -n ns-app3

# Verify NodePort Services
kubectl get svc -n ns-app1
kubectl get svc -n ns-app2
kubectl get svc -n ns-app3
```

## Step-16: Verify External DNS Log
```t
# Verify External DNS logs
kubectl logs -f $(kubectl get po | egrep -o 'external-dns[A-Za-z0-9-]+')
```

## Step-17: Verify Route53
- Go to Services -> Route53
- You should see **Record Set** added for 
  - crossns.architectprashant.com


## Step-18: Access Application using newly registered DNS Name
- Perform nslookup tests before accessing Application
- Test if our new DNS entries registered and resolving to an IP Address
```t
# nslookup commands
nslookup crossns.architectprashant.com
```
## Step-19: Access Application 
```t
# Access App1
http://crossns.architectprashant.com/app1/index.html

# Access App2
http://crossns.architectprashant.com/app2/index.html

# Access Default App (App3)
http://crossns.architectprashant.com
```


## Step-20: Clean-Up Ingress
```t

# Terraform Destroy
terraform apply -destroy -auto-approve

```

