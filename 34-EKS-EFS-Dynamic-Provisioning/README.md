---
title: AWS EKS EFS Dynamic Provisioning with Terraform
description: Automate AWS EKS Kubernetes EFS Dynamic Provisioning with Terraform
---

## Step-01: Introduction
- Implement and Test EFS Dynamic Provisioning Usecase



## Step-02: c4-02-storage-class.tf
```t
# Resource: Kubernetes Storage Class
resource "kubernetes_storage_class_v1" "efs_sc" {  
  metadata {
    name = "efs-sc"
  }
  storage_provisioner = "efs.csi.aws.com"  
  parameters = {
    provisioningMode = "efs-ap"
    fileSystemId =  aws_efs_file_system.efs_file_system.id 
    directoryPerms = "700"
    gidRangeStart = "1000" # optional
    gidRangeEnd = "2000" # optional
    basePath = "/dynamic_provisioning" # optional
  }
}
```


## Step-03: Execute Terraform Commands
```t

# Terraform Initialize
terraform init

# Terraform Validate
terraform validate

# Terraform Plan
terraform plan

# Terraform Apply
terraform apply -auto-approve
```

## Step-04: Verify Kubernetes Resources
```t
# Verify Storage Class
kubectl get sc

# Verify PVC (Persistent Volume Claim)
kubectl get pvc

# Verify PV (Persistent Volume)
kubectl get pv
```

## Step-05: Verify EFS File System, Mount Targets, Network Interfaces and Security Groups
```t
# Verify EFS File System
Go to Services -> EFS -> File Systems -> efs-demo

# Verify Mount Targets
Go to Services -> EFS -> File Systems -> efs-demo -> Network Tab

# Verify Network Interfaces
Go to Services -> EC2 -> Network & Security -> Network Interfaces -> GET THE ENI ID from Mount Targets

# Security Groups
Go to Services -> EC2 -> Network & Security -> Security Groups -> hr-dev-efs-allow-nfs-from-eks-vpc
```

## Step-06: Connect to efs-write-app Kubernetes pods and Verify 
```t
# efs-write-app - Connect to Kubernetes Pod
kubectl exec --stdin --tty <POD-NAME> -- /bin/sh
kubectl exec --stdin --tty efs-write-app  -- /bin/sh
cd /data
ls
tail -f efs-dynamic.txt
```

## Step-07: Connect to myapp1 Kubernetes pods and Verify 
```t
# List Pods
kubectl get pods 

# myapp1 POD1 - Connect to Kubernetes Pod
kubectl exec --stdin --tty <POD-NAME> -- /bin/sh
kubectl exec --stdin --tty myapp1-667d8656cc-2x824 -- /bin/sh
cd /usr/share/nginx/html/efs
ls
tail -f efs-dynamic.txt

# myapp1 POD2 - Connect to Kubernetes Pod
kubectl exec --stdin --tty <POD-NAME> -- /bin/sh
kubectl exec --stdin --tty myapp1-667d8656cc-bg8bg  -- /bin/sh
cd /usr/share/nginx/html/efs
ls
tail -f efs-dynamic.txt
```

## Step-08: Access Application
```t
# Access Application
http://<CLB-DNS-URL>/efs/efs-dynamic.txt
http://<NLB-DNS-URL>/efs/efs-dynamic.txt
```

## Step-09: Clean-Up
```t

# Destroy Resources
terraform apply -destroy -auto-approve

```


## References
- [AWS IAM OIDC Connect Provider](https://docs.aws.amazon.com/eks/latest/userguide/getting-started-console.html)
- [AWS EFS CSI Driver](https://docs.aws.amazon.com/eks/latest/userguide/efs-csi.html)
- [AWS Caller Identity Datasource](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity)
- [HTTP Datasource](https://registry.terraform.io/providers/hashicorp/http/latest/docs/data-sources/http)
- [AWS IAM Role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role)
- [AWS IAM Policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy)
- [AWS EFS CSI Docker Images across Regions](https://docs.aws.amazon.com/eks/latest/userguide/add-ons-images.html)
- [To find latestEFS CSI Driver GIT Repo](https://github.com/kubernetes-sigs/aws-efs-csi-driver/)

